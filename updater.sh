#!/bin/bash
[ "$1" == "-f" ] || [ "$1" == "--force" ] && rm -rf /var/lib/triggers/
mkdir /var/lib/triggers 2>/dev/null || true
trigger(){
	BLU="\e[34;1m"
	RED="\e[31;1m"
	RES="\e[;0m"
	if [ -d $2 ] || [ -f $2 ]
	then
		current=$(date +%s -r $2)
		touch /var/lib/triggers/$1
		last=$(cat /var/lib/triggers/$1)
		if [ "$current" != "$last" ] ; then
			echo -e "${BLU}Processing trigger for ${RES}$1"
			$3 &> /dev/null
			current=$(date +%s -r $2)
			echo "$current" > /var/lib/triggers/$1
		fi
	else
		echo -e "${RED}No such file or directory:${RES} $2"
		echo -e "${RED}Skiping trigger: ${RES}$1"
	fi
}
trigger fonts /usr/share/fonts "fc-cache -f"
trigger glib-schema /usr/share/glib-2.0/schemas/ "glib-compile-schemas /usr/share/glib-2.0/schemas/"
for i in $(ls /usr/share/icons/)
do
	trigger icon-$i /usr/share/icons/$i "gtk-update-icon-cache /usr/share/icons/$i"
done
trigger desktop-database /usr/share/applications "update-desktop-database /usr/share/applications"
trigger mandb /usr/share/man "mandb cache-update"
for i in $(ls /lib/modules/)
do
	trigger kernel-$i /lib/modules/$i "depmod -a $i"
done
trigger gdk-pixbuf /usr/lib/gdk-pixbuf-2.0/ "gdk-pixbuf-query-loaders --update-cache"
trigger mime /usr/share/mime "update-mime-database /usr/share/mime"
trigger dbus /usr/share/dbus-1 "dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBUS.ReloadConfig"
trigger eudev /lib/udev/ "udevadm control --reload"
trigger gio-modules /usr/lib/gio/modules/ "gio-querymodules /usr/lib/gio/modules/"
trigger appstream /var/cache/app-info "appstreamcli refresh-cache --force"
trigger ca-certficates /etc/ssl/certs "update-ca-certificates --fresh"
trigger cracklib /usr/share/cracklib/ "create-cracklib-dict /usr/share/cracklib/*"

