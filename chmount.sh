#!/bin/sh
[ $UID -eq 0 ] || echo -e "You must be root!" && exit 1
bb=$(which busybox)
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
if [ ! -f $1/proc/cpuinfo ] ; then
  for i in /dev /sys /proc /run
  do
    $bb mount -B /$i "$1/$i"
  done
fi
$bb chroot $1 /bin/bash
